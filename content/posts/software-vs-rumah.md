+++
title = "Mengapa Membuat Software Tidak Sama dengan Membuat Rumah?"
date = 2018-04-17T06:51:05+07:00
draft = false
image = "img/building_house.jpg"
showonlyimage = false
categories = [ "Agile Development"]
weight = 1
description = "Individual meta description for this post"
+++

Salah satu analogi yang paling sering dikeluarkan (saya juga mungkin salah satu pelakunya) saat menjelaskan tentang membuat sebuah software/aplikasi perangkat lunak, adalah membuat rumah. Proses membuat sebuah software disamakan dengan proses membuat sebuah rumah. Lanjutan dari analogi tersebut bisa jadi senada dengan kalimat berikut

<!--more-->

> "Pertama-tama buat fondasinya dulu, lalu kerangkanya, lalu dindingnya, lalu tambahkan perabot, cat..." 

dan seterusnya, mungkin Anda mengerti arah pembicaraan ini.

Permasalahannya, ini analogi yang sebenarnya berbahaya ketika dikeluarkan tanpa konteks dan semata-mata memetakan 1-1 proses dalam membuat rumah ke membuat software. Penyebabnya tentu jelas, membuat software tidak sama dengan membuat rumah.

Berbeda dengan rumah yang memiliki properti fisik yang jelas, software adalah sebuah medium yang abstrak dan tak berbentuk. Bisakah membayangkan bentuk sebuah software? Saya tidak dan saya yakin Anda juga tidak. Perbedaan yang begitu esensial, berkontribusi banyak pada bagaimana kedua benda ini dikembangkan. Membuat sebuah rumah dari dasar dan kerangkanya sebelum lanjut ke menambahkan isi rumah seperti jendela, pintu dan sebagainya, adalah sebuah alur yang logis. Dan ketika bentuk dasarnya terlihat, maka kemungkinan penambahan sebuah objek fisik bisa diterka dengan cukup akurat. Misal: tidaklah mungkin membuat pintu dengan lebar 150 cm, jika lorong tempat pintu tersebut ditempatkan, hanya memiliki lebar 95 cm. Kalau dipaksakan, maka pintu tidak bisa dibuka penuh dan mungkin menyusahkan penghuni rumah tersebut. Hal-hal seperti ini bisa diukur dan dampaknya bisa diperkirakan di atas kertas dengan cukup baik, oleh si pembuat rumah maupun calon penghuninya.

Sementara itu, karena software bersifat abstrak, maka agak sulit untuk bisa memprediksi dengan cukup presisi dampak dari penambahan sebuah fitur. Lebih parah lagi, ruang lingkup dari penambahan komponen seperti ini juga bisa jadi tidak semudah yang dibayangkan. Lagi-lagi karena agak sulit membayangkan apa itu sebenarnya sebuah software. Efek dari ini adalah pengembang software seringkali kesulitan mengimplementasikan fitur yang diinginkan oleh pengguna software tersebut. Apa yang oleh pengguna dibayangkan sebagai sebuah sesuatu yang mudah, ternyata begitu dibuat, baru terasa susah. Dan ini bukan karena pengguna software adalah orang yang awam. Pengembang software pun seringkali salah mengestimasikan bobot sebuah pekerjaan. Yang nampak straightforward untuk dibuat ternyata sulit untuk dianggap selesai karena ada kendala di integrasi dengan komponen lain di dalam software. Sebuah masalah yang tidak bisa diduga sebelumnya. Makin kompleks fitur-fitur sebuah software, makin tidak bisa diterka pengembangannya.

Oleh karena itu, untuk menyikapi ini, para pengembang software merumuskan metode pengembangan software yang iteratif, yang bisa menghantarkan value ke penggunanya dengan fitur seminimal mungkin. Dengan membatasi fitur ini, maka ketidakpastian dalam pengembangan sebuah software bisa dengan lebih mudah ditangani, dan bahkan bukan memaksanya menjadi mudah diprediksikan - satu-satunya yang pasti dalam pengembangan software adalah ketidakpastian. Pendekatan ini menarik, karena ia menghargai software sebagai sebuah medium yang abstrak lagi fleksibel, yang bisa dibentuk sesuai keinginan, tanpa perlu memenuhi kerangka yang sekaku rumah. 

Dengan demikian, jika tetap ingin menggunakan analogi pembuatan rumah, maka analogi yang tepat justru seperti ini:

> kemah - gubuk - rumah kayu - rumah bata - ...

Valuenya tetap sama: membuat tempat tinggal, bentuknya: disesuaikan dengan kompleksitas yang sesuai dengan durasi pembuatan. 

Dan satu lagi, karena saat sebuah software dijalankan di dalam komputer, ada begitu banyak proses yang berjalan. Seringkali proses-proses ini tidak bisa diterka baik di akhir pengembangan maupun di awal software tersebut dirilis, sehingga yang bisa dilakukan untuk memperbaiki kesalahan yang mungkin muncul adalah dengan melakukan pengembangan terus-menerus selama software tersebut digunakan. Hingga kemudian lahirlah perbaikan untuk error/bug tertentu, hingga penambahan fitur. 

Sekarang, lihat kembali rumah yang Anda tinggali. Kapankah terakhir kali rumah tersebut direnovasi? Bisakah Anda bayangkan tinggal di dalam rumah yang bangunannya terus-menerus diperbaiki? ;)