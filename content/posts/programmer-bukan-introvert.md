+++
title = "Programmer bukan Manusia Introvert"
date = 2018-06-19T08:33:00+07:00
draft = false
image = "img/laptop-bw.jpeg"
showonlyimage = false
categories = [ "Agile Development"]
weight = 1
description = "Individual meta description for this post"
+++

Salah satu miskonsepsi yang paling sering saya dengar tentang programmer adalah “programmer itu introvert”. Kesalahpahaman yang menyebabkan pelabelan stereotip ini ternyata menghasilkan dampak buruk dalam jangka panjang. Salah satunya, adalah organisasi yang memperlakukan programmer sebagai manusia tertutup, yang cukup diberikan internet kencang dan mungkin earphone bagus, lalu mereka dibiarkan ngoding setiap harinya.

Padahal yang sebenarnya terjadi, bukan itu. Mudah melabeli programmer sebagai introvert, karena natur pekerjaan mereka yang mengharuskan duduk diam sendirian, mengetik di depan layar komputer. Tapi apakah itu menjadikan mereka introvert? Tentu saja tidak. Sebaliknya, justru programmer harus dituntut untuk keluar dari depan komputer, berinteraksi dengan orang lain, baik dalam rangka memperbaiki kerja tim programmer, maupun untuk mendapatkan konteks bisnis dari produk yang sedang mereka buat. Pemahaman konteks yang baik, akan membawa kemajuan jauh dalam program yang mereka buat.

> Tapi, programmer di kantor saya, senengnya diisolasi di ruangan sendiri, terus asik sendiri di depan laptopnya”

Justru itulah tantangan dalam sebuah organisasi. Saya berpendapat bahwa programmer haruslah berkembang dengan lebih memahami konteks bisnis dan itu tidak akan terjadi jika mereka tidak berinteraksi dengan orang lain. Dan ini perlu dibuat alur kerja yang ajeg di dalam organisasi itu sendiri. Ada kala mereka harus berdiskusi dengan calon user namun ada saatnya mereka harus konsentrasi mengetik barisan kode, tanpa diganggu interupsi dari pihak luar.

Ujungnya, ini bisa terjadi jika ada pemahaman bersama tentang nilai seorang programmer di dalam sebuah organisasi. Programmer, sebagai pihak yang menawarkan keterampilan dirinya, perlu bisa mengkomunikasikan apa saja yang bisa ia bantu untuk meningkatkan nilai sebuah organisasi. Organisasi, sebagai pihak yang mempekerjakan programmer, perlu bisa menilai bagaimana seorang programmer bisa tumbuh bersama organisasi itu. Kebiasaan memperlakukan programmer sebagai kuli kode yang semata menerima pesanan lantas ditagih hasilnya, perlu dihentikan. Sebaliknya, komunikasi dua arah yang baik, harus terus dipelihara.

