+++
title = "Merayakan Kemenangan Kecil"
date = 2018-06-18T08:26:00+07:00
draft = false
image = "img/victory.jpeg"
showonlyimage = false
categories = [ "Agile Development"]
weight = 1
description = "Individual meta description for this post"
+++

Hidup ini bisa jadi padat sekali. Ada begitu banyak hal yang harus dikerjakan dalam waktu yang singkat. Seringkali, rasanya butuh motivasi tambahan untuk sekedar memulai sebuah hari atau memecut diri untuk mengerjakan antrian pekerjaan selanjutnya. Bagi saya, motivasi ini seringkali datang dalam wujud merayakan kemenangan kecil (small victory).

Kemenangan kecil ini, bentuknya bisa bermacam-macam, namun bagi saya, ia harus memenuhi 2 syarat: mudah dikerjakan dan dampaknya besar. Sebagai contoh, memulai pagi dengan berlari kecil keliling kompleks, bagi saya yang malas olahraga sudah menjadi kemenangan kecil. Membaca 1 artikel atau 2 bab buku yang menginspirasi kegiatan hari itu, juga sebuah kemenangan kecil. Begitu juga halnya dengan membalas email yang sudah lama duduk di kotak masuk, ataupun sekedar menulis artikel singkat seperti ini. Poinnya, sekecil apapun pencapaianmu hari ini, rayakanlah, karena untuk memulai pencapaian itu, ada banyak hal yang kamu kesampingkan dan hal yang kamu dahulukan, adalah hal yang paling berarti bagimu.

Prinsip kemenangan kecil ini juga berlaku di dunia software development. Ketika mengerjakan sebuah fitur besar yang berlarut-larut pekerjaannya, seringkali seorang developer akan merasa lelah, karena ia sedang maraton di rute yang tak berujung. Alih-alih, mengapa tidak membagi pekerjaan menjadi sesuatu yang bisa diselesaikan dalam 1–2 hari, sehingga anggota tim pun bisa merasakan pencapaian, sehingga bisa terus termotivasi menyelesaikan pekerjaan. Prinsip inilah yang kemudian diaplikasikan dalam metode agile, di mana kejaraannya adalah memberikan value sebuah software dalam tempo singkat kepada penggunanya. Sehingga baik pembuat maupun pengguna software tersebut bisa merasakan kemenangan kecil secepat-cepatnya.

Jadi, rayakan kemenangan kecilmu hari ini, pacu dirimu untuk terus berkarya dan mari kita #AgileSejakDalamPikiran.